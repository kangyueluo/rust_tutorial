fn main() {
    let tup1 = (20,"apple",3.3,false,(1,7,9),25,30,35);
	println!("{}",tup1.2);  // 3.3
	println!("{}",(tup1.4).2); // get wrap tup and print 9
	
	let tup2 = (45,6.3,"bear");
	let (a,b,c) = tup2;
	println!("a is {}",a);
	println!("b is {}",b);
	println!("c is {}",c);
	
}
