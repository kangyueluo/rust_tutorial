fn main() {
    let mut my_string = String::from("How's going? My name is Tom."); 

	// length
	println!("length: {}",my_string.len());
	
	// is Empty
	println!("String is empty? {}",my_string.is_empty());
	
	for token in my_string.split_whitespace(){
		println!("{}",token);
	}
	
	println!("Does the string contain 'Tom'? {}",my_string.contains("Tom"));
	
	my_string.push_str("Welecome to your tutorial on strings!");
	
	println!("{}", my_string);
}
