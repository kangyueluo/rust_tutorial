fn main() {
    let mut x =10;
	
	// &x 與 &mut x 不能同時存在
	let xr = &x; // a refernce x
	println!("x is {}",xr);
	
	{// run the end of code block the &mut x is gone then can print x
		let tom = &mut x;	// 透過 &mut x 來改變 x 的值
		*tom +=1;
		println!("x is {}",tom);
	}
	
	println!("x is {}",x);
	
}
