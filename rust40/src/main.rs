struct Rectangle{
	width:u8,
	height:u8
}

impl Rectangle{
	fn is_square(&self)->bool{
		self.width == self.height
	}
}

fn main() {
   
}

fn give_two()->i32{
	2
}

#[cfg(test)] // 避免編譯下面程式碼
mod ar_tests{
	
	#[test]
	#[should_panic] // 設定測試會失敗，如果失敗傳回 sucess 
	fn test_basic(){
		assert!(1 == 1); // OK
	
		panic!("Oh no!"); // 導致測試失敗
	}
	
	#[test]
	//#[ignore]
	fn test_equals(){
		assert_eq!(super::give_two(),1+1);
		assert_ne!(super::give_two(),1+2);
	}
	
	#[test]
	#[should_panic]
	fn test_structs(){
		let r = super::Rectangle{
			width: 50,
			height:25
		};
		
		assert!(r.is_square());
	}
	
	
}
