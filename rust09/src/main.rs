fn main() {
	
	let numbers = 20..61; // range is iterator
	let animals = vec!["Rabbit","Dog","Bird","Cat"];
	
	for i in 0..11{ // 不包含 11 所以只會有 0~10
		println!("The number is {}",i);
	}
	
	for i in numbers {
		println!("The number is {}",i);
	}
	
	for a in animals.iter(){
			println!("anumal name is {}",a);
	}
	
	for (index,a) in animals.iter().enumerate(){
			println!("The index is {} anumal name is {}",index,a);
	}

}
