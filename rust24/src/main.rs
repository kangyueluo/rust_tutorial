fn main() {
    let _my_vector : Vec<i32> = Vec::new(); // 為使用的 vector 需要前面加 
	let mut a_vector = vec![1,2,3,4];
	
	println!("{}",a_vector[2]);
	
	a_vector.push(49);
	a_vector.remove(1);
	
	for n in a_vector.iter(){
		println!("{}",n);
	}
}
