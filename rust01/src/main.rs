use ferris_says::say; // 引用 ferris_says的say方法， 和python的语法有点儿像
use std::io::{stdout, BufWriter}; // 标准库里引入 stdout 和 BufWriter
// main 方法， 和java，golang,c++ 类似的代码结构
fn main() {
    let stdout  = stdout(); // 实例化stdout
    let message = String::from("Hello fellow Rustaceans!"); // 照着例子抄一份，
    let width   = message.chars().count();//字符串长度 作为ferris-says 对话框宽度

    let mut writer = BufWriter::new(stdout.lock());
    say(message.as_bytes(), width, &mut writer).unwrap(); // 调用ferris_says的say方法
}