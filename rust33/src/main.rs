fn main() {
    // Replace
	{
		let my_string = String::from("Rust is fantastic");
		println!("After replace: {}", my_string.replace("fantastic","great"));
	}
	
	// Lines
	{
		let my_string = String::from("The zoo has \nrabbit\nlion\ntiger");
		
		for line in my_string.lines(){
			println!("[ {} ]",line);
		}
	}
	
	// Split
	{
		let my_string = String::from("恭喜發財+財+源+滾+滾+來");
		let tokens: Vec<&str> = my_string.split("+").collect();
		
		println!("{}",my_string);
		println!("At index 2: {}",tokens[2]);
	}
	
	// Trim
	{
		let my_string = String::from("   My name is Tom    \n\r");
		println!("Before trim: {}", my_string);
		println!("After trim: {}",my_string.trim());
	}
	
	// Chars
	{
		let my_string = String::from("greed is great");
		println!("{}",my_string);
		
		// Get character at index
		match my_string.chars().nth(4){
			Some(c) => println!("Character at index 4: {}",c),
			None => println!("No character at index 4.")
		}
	}

}
