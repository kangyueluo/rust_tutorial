use std::collections::HashMap;

fn main() {
    let mut marks = HashMap::new();
	
	// Add values
	marks.insert("Rust",96);
	marks.insert("History",80);
	marks.insert("Chinese",90);
	marks.insert("Math",70);
	marks.insert("Web Dev",82);
	
	// Find length of HashMap
	println!("Subjects {}",marks.len());
	
	
	// Get a single value
	match marks.get("Math"){
		Some(score) => println!("You got {} for Math", score),
		None => println!("You did not study Math.")
	}
	
	// Remove a value
	marks.remove("History");
	println!("Subjects {}",marks.len());
	
	
	// Loop through HashMap
	for(subject,score) in &marks{
		println!("For {} you got {}%!",subject , score);
	}
	
	// Check for value
	println!("Did you study C++? {}",marks.contains_key("C++"));
	
}