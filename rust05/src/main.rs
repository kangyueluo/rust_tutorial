fn main() {
	let x =45; // i32
	let y:i64 = 45; // i64
	let z:u64 = 45; // unsign
	let f = 6.7; // f32
	let g:f64 = 6.7; // f64
	let h:f32 = 6.7; // f32
	let b:bool = false; 
    println!("Hello, world!");
}
