struct Rectangle{
	width: u32,
	height: u32 
	
}
impl Rectangle{ // &self refer to Rectangle,which is Rectangle struct;
	fn print_description(&self){
		println!("Rectangle: {} x {}", self.width , self.height);
	}
	
	fn is_square(&self)-> bool {
		self.width == self.height
	}
	
	fn area(&self)->u32{
		let area : u32 = self.width * self.height;
		return area;
	}
	
}



fn main() {
    let my_rect = Rectangle{width: 10, height: 5}; // make Rectangle object 

	my_rect.print_description(); // call function
	
	println!("Rectangle is a square: {}", my_rect.is_square());
	
	println!("Rectangle area is {}",my_rect.area());
}
