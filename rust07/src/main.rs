fn main() {
   let mut n = 0;

	loop {
		n+=1;
		
		if (n==7){ // 也可以這樣寫
			continue;  // 回到迴圈起始跳過下面的步驟
		}
				
		if n>10{
			break; // stop the loop
		}
		
		println!("The value of n is {}",n);
	}
	// break loop to here
}
