fn main() {
	// mut 代表可以改變的變數
	let mut x = 45;
	println!("The value of x is {}",x);
	x = 60;
	println!("The value of x is {}",x);
}
