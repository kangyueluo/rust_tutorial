fn main() {
    let name = String::from("Augmented Reality");
	
	println!("Character at index 1:{}",match name.chars().nth(8){
		Some(c) => c.to_string(),
		None => "No character at index 8!".to_string()
	});
	
	
	println!("Occupation is {}",match get_occupation("Augmented"){
		Some(o) => o,
		None => "No occupation found!"
	});
	
}


fn get_occupation(name: &str) -> Option<&str>{
	match name {
		"Augmented" => Some("Dev"),
		"Reality" => Some("RRR"),
		_ => None
		
	}
}

