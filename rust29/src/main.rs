fn main() {
    let number =3;
	let name = "Peter";

	match number {
		1 => println!("It is one!"),
		2 => println!("There is two of them"),
		2..=9 => println!("2 to 9 "),
		10 | 11 => println!("It is 10 or 11"),
		_ => println!("It doesn't match")
	}
	
	match name {
		"Peter" => println!("Hello Peter."),
		"Ted" => println!("Hi Ted"),
		_=> println!("Don't know your name.")
	}
}
